package layouts.sourceit.com.timer2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.timer)
    void onFirstClick(){
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.from_left, R.anim.to_right)
                .replace(R.id.container, MyTimerFragment.newInstance())
                .commit();

    }
    @OnClick(R.id.stopWatch)
    void onSecondClick(){
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.from_right, R.anim.to_left)
                .replace(R.id.container, MyStopwatchFragment.newInstance())
                .commit();

    }

}
