package layouts.sourceit.com.timer2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.renderscript.Sampler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyTimerFragment extends Fragment {

    @BindView(R.id.timerCounter)
    TextView timerCounter;

    public static MyTimerFragment newInstance() {
        MyTimerFragment timerFragment = new MyTimerFragment();
        return timerFragment;
    }

    @OnClick(R.id.timerCounter)
    void OnClick() {
        new CountDownTimer().execute();
    }

    private class CountDownTimer extends AsyncTask<Void, Integer, Void> {

        @Override
        protected void onPreExecute() {
            timerCounter.setText("Start");
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 10; i >= 0; i--) {
                try {
                    Thread.sleep(1000);
                    publishProgress(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            timerCounter.setText(Integer.toString(values[0].intValue()));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            timerCounter.setText("Done");
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.my_timer_fragment, container, false);
        ButterKnife.bind(this, root);
        return root;
    }
}
