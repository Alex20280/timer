package layouts.sourceit.com.timer2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyStopwatchFragment extends Fragment {

    public static MyStopwatchFragment newInstance() {
        MyStopwatchFragment fragment = new MyStopwatchFragment();
        return fragment;
    }

    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.stopwatchCounter)
    TextView stopwatchCounter;
    long millisUntilFinished = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.my_stopwatch_fragment, container, false);
        ButterKnife.bind(this, root);
        return root;
    }
}